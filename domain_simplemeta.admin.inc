<?php

/**
 * @file
 * Administration
 */

/**
 * domain_simplemeta settings form builder.
 */
function domain_simplemeta_settings_form($form, &$form_state) {
  $form['domain_simplemeta_form_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Add Meta Tags Form'),
    '#description' => t('If enabled, form will appear on pages'),
    '#default_value' => variable_get('domain_simplemeta_form_enable', TRUE),
    '#return_value' => TRUE,
  );
  $form['domain_simplemeta_language_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Language Support'),
    '#description' => t('If enabled, language selectbox will appear on the form. Otherwise all created metadata will be language-neutral i.e. global for all languages'),
    '#default_value' => variable_get('domain_simplemeta_language_enable', FALSE),
    '#return_value' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Menu callback; show list of saved domain_simplemeta configurations. 
 * 
 * @return string
 *   themed output
 */
function domain_simplemeta_meta_list() {
  global $_domain;
  $domain_id = $_domain['domain_id'];
  // dpm($_domain, 'Does this get loaded');
  
  $query = db_select('domain_simplemeta', 's')
    ->extend('PagerDefault')
    ->fields('s')
    ->condition('s.domain_id', $domain_id, '=')
    ->orderBy('s.sid', 'DESC')->limit(20);
  $result = $query->execute();

  $items = array();
  while ($meta = $result->fetchObject()) {
    $meta->data = unserialize($meta->data);
    $items[] = $meta;
  }

  return theme('domain_simplemeta_meta_list', array('items' => $items)) . theme('pager');
}
/**
 * Menu callback for creating new domain_simplemeta configurations.
 * 
 * @return array
 *   form structure to render
 */
function domain_simplemeta_add() {
  global $_domain;
  $domain_id = $_domain['domain_id'];
  
  $meta = new stdClass();
  $meta->data = array();
  $meta->language = '';
  $meta->domain_id = $domain_id;
  return drupal_get_form('domain_simplemeta_meta_form', $meta);
}

/**
 * domain_simplemeta deletion confirmation form builder.
 */
function domain_simplemeta_meta_delete_confirm($form, &$form_state, $meta) {
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $meta->sid,
  );

  return confirm_form($form,
    t('Are you sure you want to delete domain_simplemeta %sid?', array('%sid' => $meta->sid)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/content/domain-simplemeta/' . $meta->sid . '/edit',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * domain_simplemeta deletion confirmation form submit callback.
 */
function domain_simplemeta_meta_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    domain_simplemeta_meta_delete($form_state['values']['sid']);
    cache_clear_all('*', 'cache_domain_simplemeta', TRUE);
  }
  $form_state['redirect'] = 'admin/content/domain-simplemeta/list';
}
