The Domain Simple Meta module is a clone of the Simple Meta (http://drupal.org/project/simplemeta) module that has been
modified to wotk with the Domain Access (http://drupal.org/project/domain) module.

Domain Simple Meta adds a column to the database table that includes a reference to a domain ID and captures unique
meta information for the same pages.