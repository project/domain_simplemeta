/**
 * @file
 * Show/hide functionality for domain_simplemeta on-page form.
 */

(function ($) {
  var formHidden = false;
  Drupal.behaviors.domain_simplemeta = {
    attach: function (context) {
      var forms = $('#domain_simplemeta-meta-form.domain_simplemeta-meta-form-ajax:not(.domain_simplemeta-processed)', context);
      forms.each(function (index) {

        var form = $(this),
            close = $('<span class="form-close"></span>').prependTo(form);
        close.text(Drupal.t('Domain Meta'));
        if (!formHidden) {
          form.addClass('domain_simplemeta-hidden').css({left: (-form.outerWidth()) + 'px'});
          formHidden = true;
        }

        close.click(function (event) {
          var $this = $(this);
          if (form.hasClass('domain_simplemeta-hidden')) {
            form.stop(true).animate({left: 0});
            $this.text(Drupal.t('Hide'));
          }
          else {
            form.stop(true).animate({left: -form.outerWidth()});
            $this.text(Drupal.t('Domain Meta'));
          }
          form.toggleClass('domain_simplemeta-hidden');
        });
      });
      forms.addClass('domain_simplemeta-processed');
    }
  };
})(jQuery);
