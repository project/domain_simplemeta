<?php

/**
 * @file
 * Documents available hooks of the domain_simplemeta module.
 */

/**
 * Return information about domain_simplemeta property.
 * 
 * See domain_simplemeta_domain_simplemeta_info() for example of domain_simplemeta properties
 * definition.
 * 
 * @return array
 *   An array whose keys are domain_simplemeta properties' machine names 
 *   and whose values are arrays containing the keys:
 *   - title: Human-readable name of the property.
 *   - form: domain_simplemeta edition form element callback for defined property
 *   - theme: domain_simplemeta property theming callback used to build output content,
 *     must be defined via hook_theme().
 *   - validate: (optional) additional domain_simplemeta form validation callback.
 *   - submit: (optional) additional domain_simplemeta form submission callback.
 * 
 * @see domain_simplemeta_domain_simplemeta_info()
 */
function hook_domain_simplemeta_info() {
  $info = array();
  $info['description'] = array(
    'title' => t('Description'),
    'form' => 'domain_simplemeta_form_description',
    'validate' => NULL,
    'submit' => NULL,
    'theme' => 'domain_simplemeta_meta_description',
  );
  return $info;
}

/**
 * Respond to performed on domain_simplemeta data actions.
 * 
 * @param object $meta 
 *   meta object operation is performed on.
 * @param string $op 
 *   performed operation, possible values are:
 *   - insert: domain_simplemeta data has just been inserted to DB
 *   - update: domain_simplemeta data has just been updated in DB
 *   - delete: domain_simplemeta data has just been removed from DB
 */
function hook_domain_simplemeta($meta, $op) {
  // Do something.
}
